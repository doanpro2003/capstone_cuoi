import React from "react";
import Footer from "../Component/Footer/Footer";
import HeaderMenu from "../Component/HeaderMenu/HeaderMenu";

export default function Layout({ children }) {
  return (
    <div>
      <HeaderMenu />
      {children}
      <Footer />
    </div>
  );
}
