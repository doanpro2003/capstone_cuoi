import React from "react";
import { useDispatch } from "react-redux";
import { setLoadingOff } from "../../redux-toolkit/loadingSlice";

export default function NotFoundPage() {
  let dispatch = useDispatch();
  dispatch(setLoadingOff());
  return (
    <div className="flex flex-col justify-center items-center h-screen w-screen bg-black text-yellow-100 ">
      <div className="animate-ping font-medium text-3xl">
        {" "}
        <p>404</p> <p>NOT FOUND PAGE</p>
      </div>
    </div>
  );
}
