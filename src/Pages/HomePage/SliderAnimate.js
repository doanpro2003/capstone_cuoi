import React from "react";
import Lottie from "lottie-react";
import eAnimation from "../../assets/131888-e-learning.json";
import elAnimation from "../../assets/12546-welcome.json";
import { Slide } from "react-awesome-reveal";
export default function SliderAnimate() {
  return (
    <div
      style={{ paddingTop: "6.5rem" }}
      className=" bg-white h-screen w-screen z-50 block grid-cols-2 shadow-xl shadow-red-200/50 lg:grid md:block m-auto"
    >
      <div className="flex flex-col justify-center items-center">
        <Slide duration={400} direction="down" triggerOnce>
          <Lottie className="justify-center align-center" animationData={elAnimation} loop={true} />
        </Slide>
      </div>


      <Slide duration={2000} direction="down" triggerOnce className="m-24" >
        <Lottie animationData={eAnimation} loop={true} />
      </Slide>
    </div>
  );
}
