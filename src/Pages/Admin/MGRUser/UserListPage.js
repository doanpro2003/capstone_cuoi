import React, { useEffect, useState } from "react";
import { message, Table } from "antd";
import {
  deleteUser,
  getFindUser,
  getListUser,
} from "./../../../service/userService";
import { userLocalStoreChange } from "../../../service/localService";
import { useNavigate, useParams } from "react-router-dom";
import Search from "antd/es/input/Search";
import { useDispatch, useSelector } from "react-redux";
import { setUserFind } from "../../../redux-toolkit/userSlice";
const columns = [
  {
    title: "Tài Khoản",
    dataIndex: "taiKhoan",
    key: "name",
    render: (text) => <a>{text}</a>,
  },
  {
    title: "Họ Tên",
    dataIndex: "hoTen",
    key: "age",
  },
  {
    title: "Email",
    dataIndex: "email",
    key: "address",
  },
  {
    title: "Số Điện Thoại",
    dataIndex: "soDt",
    key: "soDT",
  },
  {
    title: "Loại Người Dùng",
    dataIndex: "maLoaiNguoiDung",
    key: "maLoaiNguoiDung",
  },
  {
    title: "Action",
    dataIndex: "action",

    key: "action",
  },
];
export default function UserListPage() {
  // let [dataArr, setDataArr] = useState([]);
  const [userArr, setuserArr] = useState([]);
  const newUserArr = () => {
    return userArr.map((item) => {
      return {
        ...item,
        action: (
          <>
            <button
              onClick={() => {
                window.location.href = `/admin/mrgregiscourse/${item.taiKhoan}`;
              }}
              className="border-2 border-black rounded-md mx-1 px-2 py-1"
            >
              ghi danh
            </button>
            <button
              onClick={() => {
                deleteUser(item.taiKhoan)
                  .then((res) => {
                    message.success("xóa thành công");
                    window.location.reload();
                    console.log(res);
                  })
                  .catch((err) => {
                    message.error("xóa thất bại");
                    console.log(err);
                  });
              }}
              className="border-2 border-black rounded-md mx-1 px-2 py-1"
            >
              xóa
            </button>
            <button
              onClick={() => {
                window.location.href = `/admin/mgrformuserupdate/${item.taiKhoan}`;
              }}
              className="border-2 border-black rounded-md mx-1 px-2 py-1"
            >
              sửa
            </button>
          </>
        ),
      };
    });
  };

  useEffect(() => {
    getListUser()
      .then((res) => {
        // console.log(res);
        setuserArr(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const newUserArrFind = () => {
    return pennant?.map((item) => {
      return {
        ...item,
        action: (
          <>
            <button
              onClick={() => {
                window.location.href = `/admin/mrgregiscourse/${item.taiKhoan}`;
              }}
              className="border-2 border-black rounded-md mx-1 px-2 py-1"
            >
              ghi danh
            </button>
            <button
              onClick={() => {
                deleteUser(item.taiKhoan)
                  .then((res) => {
                    message.success("xóa thành công");
                    window.location.reload();
                    console.log(res);
                  })
                  .catch((err) => {
                    message.error("xóa thất bại");
                    console.log(err);
                  });
              }}
              className="border-2 border-black rounded-md mx-1 px-2 py-1"
            >
              xóa
            </button>
            <button
              onClick={() => {
                window.location.href = `/admin/mgrformuserupdate/${item.taiKhoan}`;
              }}
              className="border-2 border-black rounded-md mx-1 px-2 py-1"
            >
              sửa
            </button>
          </>
        ),
      };
    });
  };
  let dispatch = useDispatch();
  // console.log("pennant", pennant);
  const onSearch = (value) => {
    console.log(value);
    getFindUser(value)
      .then((res) => {
        console.log("sadfasdf", res);
        dispatch(setUserFind(res.data));
      })
      .catch((err) => {
        message.error("Không tìm thấy User");

        console.log(err);
      });
  };
  let pennant = useSelector((state) => {
    return state.userSlice.userFind;
  });
  console.log("pennant", pennant);
  let renderUser = () => {
    if (pennant.length > 0) {
      return newUserArrFind();
    } else {
      return newUserArr();
    }
  };
  // console.log("renderUser", renderUser());
  return (
    <div className="text-left">
      <a
        className="text-2xl underline font-semibold"
        href="/admin/mgrformusernew"
      >
        THÊM NGƯỜI DÙNG
      </a>
      <Search
        className="bg-green-500 mt-10"
        placeholder="Tìm kiếm người dùng"
        allowClear
        enterButton="tìm kiếm"
        size="large"
        onSearch={onSearch}
      />
      <Table columns={columns} dataSource={renderUser()} />;
    </div>
  );
}
