export const USERLOCAL = "USERLOCAL";
export const USER_CHANGE = "USER_CHANGE";

export const userLocalService = {
  get: () => {
    let userJson = localStorage.getItem(USERLOCAL);
    if (userJson) {
      return JSON.parse(userJson);
    } else {
      return null;
    }
  },
  set: (data) => {
    let userJson = JSON.stringify(data);
    localStorage.setItem(USERLOCAL, userJson);
  },
  remote: () => {
    localStorage.removeItem(USERLOCAL);
  },
};
///////////////
export const userLocalStoreChange = {
  get: () => {
    let userJson = localStorage.getItem(USER_CHANGE);
    if (userJson) {
      return JSON.parse(userJson);
    } else {
      return [];
    }
  },
  set: (data) => {
    let userJson = JSON.stringify(data);
    localStorage.setItem(USER_CHANGE, userJson);
  },

  remove: () => {
    localStorage.removeItem(USER_CHANGE);
  },
};
